package rgo.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import rgo.Application;
import rgo.domain.Address;
import rgo.domain.Company;
import rgo.modules.AddressDetectionModule;
import rgo.repositories.AddressRepository;
import rgo.repositories.CompanyRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@Transactional
public class AddressMonitoringServiceTest {

    @Autowired
    AddressMonitoringService addressMonitoringService;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Test
    public void testCheckAddressChangesForCompanies() throws Exception {
        List<Address> updatedAddresses = addressMonitoringService.checkAddressChangesForCompanies();
        assertEquals(4, updatedAddresses.size());
    }

    @Test
    public void testGetAddressProcessingModule() throws Exception {

        Company regis24 = companyRepository.findOne(1L);
        Company savagewear = companyRepository.findOne(2L);
        Company powerflasher = companyRepository.findOne(3L);
        Company idealo = companyRepository.findOne(4L);

        AddressDetectionModule regis24DetectionModule = addressMonitoringService.getAddressProcessingModule(getLatestAddressForCompany(regis24));
        assertEquals(AddressDetectionModule.REGIS24,regis24DetectionModule.getName());

        //no module defined for savagewear, should use default
        AddressDetectionModule savagewearDetectionModule = addressMonitoringService.getAddressProcessingModule(getLatestAddressForCompany(savagewear));
        assertEquals(AddressDetectionModule.DEFAULT,savagewearDetectionModule.getName());

        //powerflasher module defined for powerflasher is disabled, should use default
        AddressDetectionModule powerflasherDetectionModule = addressMonitoringService.getAddressProcessingModule(getLatestAddressForCompany(powerflasher));
        assertEquals(AddressDetectionModule.DEFAULT,powerflasherDetectionModule.getName());

        //idealo module name is corrupted in DB, should use default
        AddressDetectionModule idealoDetectionModule = addressMonitoringService.getAddressProcessingModule(getLatestAddressForCompany(idealo));
        assertEquals(AddressDetectionModule.DEFAULT,idealoDetectionModule.getName());
    }

    private Address getLatestAddressForCompany(Company company) {
        return addressRepository.findFirstByCompanyOrderByDetectionDateDesc(company);
    }
}