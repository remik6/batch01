package rgo.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import rgo.Application;
import rgo.domain.Address;
import rgo.domain.Company;
import rgo.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@Transactional
public class RepositoriesTest {

    @Autowired
    CompanyRepository companyRepo;

    @Autowired
    ProcessingModuleRepository processingModuleRepo;

    @Autowired
    AddressRepository addressRepository;

    @Test
    public void testCompanyRepository() {
        List<Company> companies = companyRepo.findAll();
        assertEquals(4,companies.size());
    }

    @Test
    public void testProcessingModuleRepository() {
        assertEquals(2, processingModuleRepo.findByActiveFlagTrue().size());
    }

    @Test
    public void testAddressRepository() {

        final String MOST_RECENT = "MostRecentStraße 3, 10123 Berlin";

        Company c1 = new Company("Company 1", "c1@gmx.de", "c1.de");
        companyRepo.save(c1);

        List<Address> addresses = new ArrayList<Address>();
        addresses.add(new Address("OldStraße 1, 10123 Berlin",c1, DateUtils.toDate(2014, 10, 22)));
        addresses.add(new Address("ABitFresherStraße 2, 10123 Berlin",c1, DateUtils.toDate(2015, 12, 8)));
        addresses.add(new Address(MOST_RECENT,c1, DateUtils.toDate(2016, 1, 22)));
        addressRepository.save(addresses);

        assertEquals(MOST_RECENT, addressRepository.findFirstByCompanyOrderByDetectionDateDesc(c1).getCombined());
    }

    /**
     * assuming that the data.sql is used and each of four companies have 3 addresses already in DB and the latest ones
     * are in late 2016
     */
    @Test
    public void testFetchingQuery() {
        List<Address> latest = addressRepository.findLatestAddressesForAllCompanies();
        assertEquals(4, latest.size());
        for (Address a : latest) {
            //each valid latest check existing in db is in 2016
            assertTrue(a.getDetectionDate().after(DateUtils.toDate(2016,1,1)));
        }
    }



}