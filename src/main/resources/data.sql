
INSERT INTO processing_module(name, active_flag, default_flag) VALUES ('Regis24DetectionModule', true, false); -- ID: 1
INSERT INTO processing_module(name, active_flag, default_flag) VALUES ('PowerFlasherDetectionModule_disabled', false, false); -- ID: 2
INSERT INTO processing_module(name, active_flag, default_flag) VALUES ('IdealoDetectionModule_WrongTestValueHere', true, false); -- ID: 3

INSERT INTO company(name, notification_email, url, module_processing_module_id) VALUES ('regis24', 'regis-notify@gmx.de','http://www.regis24.de/impressum.php',1);
INSERT INTO company(name, notification_email, url) VALUES ('Savage Wear', 'savagewear-notify@gmx.de','http://www.savage-wear.com/impressum/index.html');
INSERT INTO company(name, notification_email, url,module_processing_module_id) VALUES ('Powerflasher', 'poswerflasher-notify@gmx.de','http://www.powerflasher.de/#/de/kontakt',2);
INSERT INTO company(name, notification_email, url,module_processing_module_id) VALUES ('Idealo', 'idealo-notify@gmx.de','http://www.idealo.de/preisvergleich/AGB.html',3);


insert into address(combined, detection_date, company_id) values ('regis_very_old_address', {ts '2012-09-17 18:47:52.55'},1);
insert into address(combined, detection_date, company_id) values ('regis_old_address', {ts '2014-10-10 13:11:11.01'},1);
insert into address(combined, detection_date, company_id) values ('regis_recent_address', {ts '2016-01-10 09:22:33.44'},1);

insert into address(combined, detection_date, company_id) values ('savageWear_very_old_address', {ts '2012-09-17 18:47:52.55'},2);
insert into address(combined, detection_date, company_id) values ('savageWear_old_address', {ts '2014-10-10 13:11:11.01'},2);
insert into address(combined, detection_date, company_id) values ('savageWear_recent_address', {ts '2016-01-10 09:22:33.44'},2);

insert into address(combined, detection_date, company_id) values ('Powerflasher_very_old_address', {ts '2012-09-17 18:47:52.55'},3);
insert into address(combined, detection_date, company_id) values ('Powerflasher_old_address', {ts '2014-10-10 13:11:11.01'},3);
insert into address(combined, detection_date, company_id) values ('Powerflasher_recent_address', {ts '2016-01-10 09:22:33.44'},3);

insert into address(combined, detection_date, company_id) values ('Idealo_very_old_address', {ts '2012-09-17 18:47:52.55'},4);
insert into address(combined, detection_date, company_id) values ('Idealo_old_address', {ts '2014-10-10 13:11:11.01'},4);
insert into address(combined, detection_date, company_id) values ('Idealo_recent_address', {ts '2016-01-10 09:22:33.44'},4);
