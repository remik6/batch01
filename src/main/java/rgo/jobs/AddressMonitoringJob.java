package rgo.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rgo.domain.Address;
import rgo.services.AddressMonitoringService;
import rgo.services.AddressNotificationService;

import java.util.List;

@Service(AddressMonitoringJob.BEAN_NAME)
public class AddressMonitoringJob {

    public final static String BEAN_NAME = "AddressMonitoringJob";
    public final static String METHOD_NAME = "runAddressMonitoring";

    private static final Logger logger = LoggerFactory.getLogger(AddressMonitoringJob.class);

    @Autowired
    AddressMonitoringService addressMonitoringService;

    @Autowired
    AddressNotificationService addressNotificationService;

    public void runAddressMonitoring() {
        logger.info("Started AddressMonitoringJob");
        List<Address> updatedAddresses = addressMonitoringService.checkAddressChangesForCompanies();
        addressNotificationService.notifyAll(updatedAddresses);
        logger.info("Finished AddressMonitoringJob");
    }
}
