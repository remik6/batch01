package rgo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rgo.domain.Address;
import rgo.jobs.AddressMonitoringJob;
import rgo.services.AddressMonitoringService;
import rgo.services.AddressNotificationService;

import java.util.List;

/**
 * Implemented just for easy testing of the address monitoring service
 */

@RestController
public class AddressMonitoringController {

    private static final Logger logger = LoggerFactory.getLogger(AddressMonitoringController.class);

    @Autowired
    AddressMonitoringService addressMonitoringService;

    @Autowired
    AddressNotificationService addressNotificationService;

    @RequestMapping("/update")
    public List<Address> update() {
        logger.info("Started update");
        List<Address> updatedAddresses = addressMonitoringService.checkAddressChangesForCompanies();
        addressNotificationService.notifyAll(updatedAddresses);
        logger.info("Finished update");
        return updatedAddresses;
    }
}
