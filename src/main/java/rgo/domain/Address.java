package rgo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @NotNull
    private String combined;

    @ManyToOne
    @NotNull
    private Company company;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date detectionDate;

    protected Address() {
    }

    public Address(String combined, Company company, Date detectionDate) {
        this.combined = combined;
        this.company = company;
        this.detectionDate = detectionDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCombined() {
        return combined;
    }

    public void setCombined(String combined) {
        this.combined = combined;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getDetectionDate() {
        return detectionDate;
    }

    public void setDetectionDate(Date detectionDate) {
        this.detectionDate = detectionDate;
    }
}
