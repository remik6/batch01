package rgo.domain;

import javax.persistence.*;

@Entity
public class Company {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @OneToOne
    private ProcessingModule module;

    private String name;

    private String notificationEmail;

    private String url;

    protected Company() {}

    public Company(String name, String notificationEmail, String url) {
        this.name = name;
        this.notificationEmail = notificationEmail;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ProcessingModule getModule() {
        return module;
    }

    public void setModule(ProcessingModule module) {
        this.module = module;
    }

}
