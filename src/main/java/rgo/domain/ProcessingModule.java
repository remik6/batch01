package rgo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class ProcessingModule {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name= "PROCESSING_MODULE_ID")
    private long id;

    @NotNull
    private String name;

    private boolean activeFlag = true;

    private boolean defaultFlag = true;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public boolean isDefaultFlag() {
        return defaultFlag;
    }

    public void setDefaultFlag(boolean defaultFlag) {
        this.defaultFlag = defaultFlag;
    }
}
