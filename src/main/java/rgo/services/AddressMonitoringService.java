package rgo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rgo.domain.Address;
import rgo.modules.AddressDetectionModule;
import rgo.repositories.AddressRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressMonitoringService {

    private static final Logger logger = LoggerFactory.getLogger(AddressMonitoringService.class);

    @Autowired
    BeanFactory beanFactory;

    @Autowired
    AddressRepository addressRepository;

    public List<Address> checkAddressChangesForCompanies() {

        logger.info("checkAddressChangesForCompanies() :: Started");

        List<Address> updatedAddresses = new ArrayList<>();
        List<Address> latestAddresses = addressRepository.findLatestAddressesForAllCompanies();

        for (Address address : latestAddresses ) {

            AddressDetectionModule addressDetectionModule = getAddressProcessingModule(address);
            logger.info("Running "+addressDetectionModule.getName()+ " for "+ address.getCompany().getName());
            Address updatedAddress = addressDetectionModule.checkCompanyAddress(address.getCompany());
            String result = updatedAddress==null?"Address not updated":"Address updated to : "+updatedAddress.getCombined();
            logger.info("Finished. "+ result );

            if (updatedAddress != null) {
                addressRepository.save(updatedAddress);
                updatedAddresses.add(updatedAddress);
            }
        }
        logger.info("Finished. Updated addresses : "+updatedAddresses.size());
        return updatedAddresses;
    }

    public AddressDetectionModule getAddressProcessingModule(Address address) {

        AddressDetectionModule addressDetectionModule = null;

        //checking if company has defined processing module and if it's active
        if (address.getCompany().getModule() != null && address.getCompany().getModule().isActiveFlag() ) {
            try {
                addressDetectionModule = (AddressDetectionModule)beanFactory.getBean(address.getCompany().getModule().getName());
            } catch (NoSuchBeanDefinitionException ex) {
                logger.error(ex.getMessage());
            }
        }

        //if detection module is empty or inactive or with wrong name select the default one
        if (addressDetectionModule == null) {
            addressDetectionModule = (AddressDetectionModule)beanFactory.getBean(AddressDetectionModule.DEFAULT);
        }
        return addressDetectionModule;
    }
}