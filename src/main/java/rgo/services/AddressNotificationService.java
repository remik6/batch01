package rgo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import rgo.domain.Address;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class AddressNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(AddressNotificationService.class);

    @Value("${mail.testMailAddress}")
    String testMailAddress;

    @Autowired
    private JavaMailSender javaMailSender;

    public void notifyAll(List<Address> addresses) {
        logger.info("Started sendingEmailNotifications ...");
        for (Address updatedAddress : addresses) {
            notify(updatedAddress);
        }
        logger.info("... Finished sendingEmailNotifications");
    }

    public void notify(Address updatedAddress) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {

                mimeMessage.setRecipient(Message.RecipientType.TO,
                        //new InternetAddress(updatedAddress.getCompany().getNotificationEmail()));
                        new InternetAddress(testMailAddress));
                mimeMessage.setSubject("Address was updated !");
                mimeMessage.setText("Your address has been successfully updated on "+updatedAddress.getDetectionDate()+ " to "+updatedAddress.getCombined());
            }
        };

        try {
            this.javaMailSender.send(preparator);
        }
        catch (MailException ex) {
            logger.error(ex.getMessage());
        }
    }

    public String getTestMailAddress() {
        return testMailAddress;
    }

    public void setTestMailAddress(String testMailAddress) {
        this.testMailAddress = testMailAddress;
    }
}
