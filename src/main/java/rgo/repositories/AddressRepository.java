package rgo.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rgo.domain.Address;
import rgo.domain.Company;

import java.util.List;

@Repository
public interface AddressRepository extends PagingAndSortingRepository<Address, Long> {

    List<Address> findAll();

    Address findFirstByCompanyOrderByDetectionDateDesc(Company company);

    @Query("SELECT a FROM Address a WHERE a.detectionDate = (SELECT MAX(aa.detectionDate) FROM Address aa WHERE aa.company = a.company)")
    List<Address> findLatestAddressesForAllCompanies();
}
