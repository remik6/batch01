package rgo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rgo.domain.ProcessingModule;

import java.util.List;

@Repository
public interface ProcessingModuleRepository  extends JpaRepository<ProcessingModule, Long> {

    List<ProcessingModule> findByActiveFlagTrue();
}
