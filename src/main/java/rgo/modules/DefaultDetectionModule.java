package rgo.modules;

import org.springframework.stereotype.Component;
import rgo.domain.Address;
import rgo.domain.Company;

import java.util.Date;

@Component(AddressDetectionModule.DEFAULT)
public class DefaultDetectionModule implements AddressDetectionModule {

    @Override
    public Address checkCompanyAddress(Company company) {

        //for testing purposes I assume that each time for default module the address is updated
        Address updatedAddress = new Address("address updated by DefaultDetectionModule for "+company.getName(), company, new Date());
        return updatedAddress;
    }

    @Override
    public String getName() {
        return AddressDetectionModule.DEFAULT;
    }
}
