package rgo.modules;

import rgo.domain.Address;
import rgo.domain.Company;

public interface AddressDetectionModule {

    final String DEFAULT = "DefaultDetectionModule";
    final String REGIS24 = "Regis24DetectionModule";
    final String IDEALO = "IdealoDetectionModule";
    final String POWERFLASHER = "PowerflasherDetectionModule";

    /**
     * @param company for which the checkup of address should be done
     * @return Address if address was updated returns new address, otherwise returns null
     */
    Address checkCompanyAddress(Company company);
    String getName();
}
