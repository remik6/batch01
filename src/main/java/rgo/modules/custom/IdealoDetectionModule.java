package rgo.modules.custom;

import org.springframework.stereotype.Component;
import rgo.domain.Address;
import rgo.domain.Company;
import rgo.modules.AddressDetectionModule;

@Component(AddressDetectionModule.IDEALO)
public class IdealoDetectionModule implements AddressDetectionModule {

    @Override
    public Address checkCompanyAddress(Company company) {
        //for testing purposes Idealo address stays the same, so returning null
        return null;
    }

    @Override
    public String getName() {
        return AddressDetectionModule.IDEALO;
    }
}
