package rgo.modules.custom;

import org.springframework.stereotype.Component;
import rgo.domain.Address;
import rgo.domain.Company;
import rgo.modules.AddressDetectionModule;

@Component(AddressDetectionModule.POWERFLASHER)
public class PowerflasherDetectionModule implements AddressDetectionModule {

    @Override
    public Address checkCompanyAddress(Company company) {
        //for testing purposes module is disabled, returning null
        return null;
    }

    @Override
    public String getName() {
        return AddressDetectionModule.POWERFLASHER;
    }
}
