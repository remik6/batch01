package rgo.modules.custom;

import org.springframework.stereotype.Component;
import rgo.domain.Address;
import rgo.domain.Company;
import rgo.modules.AddressDetectionModule;

import java.util.Date;

@Component(AddressDetectionModule.REGIS24)
public class Regis24DetectionModule implements AddressDetectionModule {

    @Override
    public Address checkCompanyAddress(Company company) {

        //for testing purposes I assume that each time for regis the address is updated
        Address updatedAddress = new Address("address updated by Regis24DetectionModule for "+company.getName(), company, new Date());
        return updatedAddress;
    }

    @Override
    public String getName() {
        return AddressDetectionModule.REGIS24;
    }
}
