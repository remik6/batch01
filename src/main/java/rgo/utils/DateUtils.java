package rgo.utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class DateUtils {

    private DateUtils(){};

    public static Date toDate(int year, int month, int day) {
        return toDate(year, month, day,ZoneOffset.UTC);
    }

    public static Date toDate(int year, int month, int day, ZoneOffset offset) {
        return Date.from(LocalDateTime.of(year, month, day, 0, 0, 0).toInstant(offset));
    }
}
